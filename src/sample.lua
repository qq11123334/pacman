---- Current Level ----
CurrentLevel = 2
local background = display.newImage(self.view, ImageDir .. CurrentLevel .. "3.png")














---- Grid generate ----

local string_grid1 = {
	"XXXXXXXXXXXXXXXXX",
	"X..............LX",
	"X.XXX.XXXXX.XXX.X",
	"X.XXX.......D.X.X",
	"X.XXX.XXKXX.X.X.X",
	"X.....XMMMX.XXX.X",
	"X.XXX.XXXXX.....X",
	"X.XXX.......XXX.X",
	"X.XXX.XX.X.XXXX.X",
	"X.XXX....X.XXXX.X",
	"X.XXL.XX........X",
	"X.XX.XXXXX.XXXX.X",
	"X.XX.XXXXX.XXXX.X",
	"XL.............LX",
	"XXXXXXXXXXXXXXXXX"
}

local example_grid = {
	"XXXX",
	"X..X",
	"XXXX"
}


local n = #string_grid[CurrentLevel]
local m = #string_grid[CurrentLevel][1]
GridSize = math.min(display.contentWidth / (m + 1), display.contentHeight / (n + 1))

for i = 1, n do
	Grid[i] = {}
	for j = 1, m do
		Grid[i][j] = string_grid[CurrentLevel][i]:sub(j, j)
	end
end


for i = 1, n do
	for j = 1, m do
		X = j * GridSize
		Y = i * GridSize
		if (Grid[i][j] == 'X') then
			-- generate block
		elseif (Grid[i][j] == 'D') then
			-- generate door
		elseif (Grid[i][j] == 'K') then
			-- generate key
		elseif (Grid[i][j] == 'L') then
			-- generate leaf
		elseif (Grid[i][j] == '.' and Grid[i][j] ~= 'M') then
			-- generate bean
		end
	end
end

for i = 1, n do
	for j = 1, m do
		X = j * GridSize
		Y = i * GridSize
		if (Grid[i][j] == 'M') then
			-- generate monster
		end
	end
end









---- move ----
function move()
	-- ...
end
MoveTimer = timer.performWithDelay(Delay, move, -1)


Delay = SecondPerStep * 1000 / FPS







---- player move


local example_grid = {
	"XXXX",
	"X..X",
	"XXXX"
}



PlayerX = 2
PlayerY = 2
DirectionX = 0
DirectionY = 0

local nextX = PlayerX + DirectionX
local nextY = PlayerY + DirectionY
if (Grid[nextY][nextX] ~= 'X') then
	Player.x = Player.x + DirectionX * (GridSize / FPS)
	Player.y = Player.y + DirectionY * (GridSize / FPS)
end



NextDirectionX = 0
NextDirectionY = 0

local function press_up(event)
	NextDirectionX = 0
	NextDirectionY = -1
end
options["onPress"] = press_up
local button_up = widget.newButton(options)




if (Grid[PlayerY + NextDirectionY][PlayerX + NextDirectionX] ~= 'X') then
	DirectionX = NextDirectionX
	DirectionY = NextDirectionY
end






---- eat bean ----
local c = Grid[PlayerY][PlayerX]
if (c == '.') then
	audio.play(bean_sound)
	Bean[PlayerY][PlayerX].isVisible = false
	Grid[PlayerY][PlayerX] = 'S'
	BeanCount = BeanCount - 1
	Score = Score + 4
	ScoreText.text = Score
end





--- win ---
if (BeanCount == 0) then
	--- win
end





---- touch monster ----
local touch = false
for i = 1, MonsterCount do
	local distance = math.abs(Monster[i].x - Player.x) + math.abs(Monster[i].y - Player.y)
	if (distance <= GridSize / 2) then
		-- touch monster

		if (LeafRemainTime > 0 and MonsterDead[i] == false) then
			-- kill monster
		elseif (MonsterDead[i] == false) then
			touch = true
		end
	end
end

if (touch) then
	-- fail
end








---- Random Direction of Monster ----

local isVertical = (Grid[MonsterY[i] - 1][MonsterX[i]] ~= 'X' or Grid[MonsterY[i] + 1][MonsterX[i]] ~= 'X')
local isHorizontal = (Grid[MonsterY[i]][MonsterX[i] - 1] ~= 'X' or Grid[MonsterY[i]][MonsterX[i] + 1] ~= 'X')

while (isVertical and isHorizontal) or (Grid[MnextY][MnextX]) == 'X' or (MonsterDirectionX[i] == 0 and MonsterDirectionY[i] == 0) do
	local rand1 = math.random(0, 1)
	local rand2 = math.random(0, 1)
	local rand3 = math.random(0, 9)
	MonsterDirectionX[i] = rand1
	MonsterDirectionY[i] = (1 - rand1)
	if (rand2 ~= 0) then
		MonsterDirectionX[i] = MonsterDirectionX[i] * -1
		MonsterDirectionY[i] = MonsterDirectionY[i] * -1
	end
	if (rand3 == 9) then
		MonsterDirectionX[i] = 0
		MonsterDirectionY[i] = 0
	end

	MnextX = MonsterX[i] + MonsterDirectionX[i]
	MnextY = MonsterY[i] + MonsterDirectionY[i]

	if Grid[MnextY][MnextX] ~= 'X' then
		break
	end
end






-- Thank You For Listening


--  _______    _                             _
-- |__   __|  | |          ___              | |
--    | |     | | ___     |_  |   _  ___    | | _     ____
--    | |     | |/ _  \    _| |  | |/ _ \   | |/ /   | ___|
--    | |     |  /   \ |  | o |  |  /  \ |  |   |    |___ |
--    |_|     |_|    | |  |___|  |_|   |_|  |_|\_\   |____|