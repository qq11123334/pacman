local composer  = require("composer")
local widget    = require("widget")
local movieclip = require("movieclip")
local physics   = require("physics")

local scene = composer.newScene()

-- 圖片與聲音的資料夾
SrcDir = "src."
ImageDir = "images/"
SoundDir = "sound/"

function scene:create(event)
	
	display.setStatusBar(display.HiddenStatusBar)
	
	-- 背景
	background = display.newImageRect(self.view, ImageDir .. "background.png", display.contentWidth, display.contentHeight)
	background.x = display.contentCenterX
	background.y = display.contentCenterY

	-- tap to play 的圖片
	-- 佔據全螢幕 四周是透明
	local tap = display.newImageRect(self.view, ImageDir .. "tap.png", display.contentWidth, display.contentHeight)
	tap.x = display.contentCenterX
	tap.y = display.contentCenterY

	-- 點擊螢幕進選單
	function tap_start(event)

		-- 點擊螢幕的音效
		local tap_sound = audio.loadSound(SoundDir .. "tap.mp3")
		audio.play(tap_sound)

		-- 退場的特效
		options = {
			time = 300,
			effect = "fade"
		}
		composer.gotoScene(SrcDir .. "level_select", options)
	end

	tap:addEventListener("tap", tap_start)
end

function scene:show(event)
	if(event.phase == "did") then

	end
end


function scene:hide(event)
	if(event.phase == "will") then
		composer.removeScene("title")
	elseif(event.phase == "did") then
	end
end

function scene:destrory(event)
	self.view:removeSelf()
	self.view = nil
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destrory", scene)

return scene