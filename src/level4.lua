local composer  = require("composer")
local widget    = require("widget")
local movieclip = require("movieclip")
local physics   = require("physics")

local scene = composer.newScene()

display.setStatusBar(display.HiddenStatusBar)

CurrentLevel = 4 -- CurrentLevel 代表是第幾關

-- 方向的表示:方向(x, y)，有上下左右四種方向，上(0, -1) 下(0, 1) 左(-1, 0) 右(0, -1)
DirectionX = 0      -- 小精靈現在面向的方向
DirectionY = 0
NextDirectionX = 0  -- 玩家最後按的方向
NextDirectionY = 0
StepCount = 0       -- 小精靈現在在第幾步，假設FPS = 30，那小精靈經過30步後(呼叫30次move()) 會走一格 
----
Grid = {}       -- 地圖的二維陣列
Bean = {}       -- 存豆子的display物件
BeanCount = 0   -- 剩餘的豆子數量
----
Monster = {}            -- 平時 怪物的display物件
MonsterWeak = {}        -- 吃到葉子時 怪物的display物件
MonsterCount = 0        -- 怪物數量
MonsterStep = 0         -- 怪物走的步數，跟StepCount同理
MonsterX = {}           -- 存怪物在第幾個row
MonsterY = {}           -- 存怪物在第幾個column
MonsterDirectionX = {}  -- 怪物的方向
MonsterDirectionY = {}
MonsterDead = {}        -- 紀錄怪物是否被吃掉了
---- 以下變數名稱同理
Leaf = {}
LeafX = {}
LeafY = {}
LeafCount = 0
LeafRemainTime = 0 -- 葉子效果的剩餘時間
----
Door = nil
DoorX = 0
DoorY = 0
----
Key = nil
KeyX = 0
KeyY = 0
----

-- 紀錄分數與顯示分數的物件
ScoreText = nil
Score = 0
----
Offest = display.contentHeight / 6 -- 用來顯示遊戲的地圖 y座標同加offest
SecondPerStep = 0.25 -- 走一格所要花的秒數
FPS = math.floor(60 * SecondPerStep) -- 一秒要呼叫幾次 move
Delay = SecondPerStep * 1000 / FPS -- move與move之間要間隔多久 用在performWithDelay

function scene:create()

    -- 實作的的關係 螢幕的長寬，一定要是 FPS 的倍數
    for i = FPS, 1, -1 do
        if (display.contentWidth % i == 0 and display.contentHeight % i == 0) then
            FPS = i
            break
        end
    end

    composer.removeScene(SrcDir .. "level_select")

    -- 1 ~ 6 關的關卡設置
    local string_grid1 = {
        "XXXXXXXXXXXXXXXXX",
        "X..............LX",
        "X.XXX.XXXXX.XXX.X",
        "X.XXX.......D.X.X",
        "X.XXX.XXKXX.X.X.X",
        "X.....XMMMX.XXX.X",
        "X.XXX.XXXXX.....X",
        "X.XXX.......XXX.X",
        "X.XXX.XX.X.XXXX.X",
        "X.XXX....X.XXXX.X",
        "X.XXL.XX........X",
        "X.XX.XXXXX.XXXX.X",
        "X.XX.XXXXX.XXXX.X",
        "XL.............LX",
        "XXXXXXXXXXXXXXXXX"
    }
    local string_grid2 = {
        "XXXXXXXXXXXXXXXXX",
        "X.....XXXXX.....X",
        "X.XXX.....L.XXX.X",
        "X.XXXLXXXXX.XXX.X",
        "X.XXX....X..XXX.X",
        "X.XXX.XX.X.XXXX.X",
        "X.....XX.X.XXXX.X",
        "XX.XXXXX........X",
        "XX.XXXXX.X.XXXX.X",
        "XL.......X.X..X.X",
        "X.XXKXX.XX.X..X.X",
        "X.XMMMX.XX.X..D.X",
        "X.XXXXX.XX.XXXX.X",
        "X..............LX",
        "XXXXXXXXXXXXXXXXX"
    }
    local string_grid3 = {
        "XXXXXXXXXXXXXXXXX",
        "X...............X",
        "XLXXXX.XXX.XXXX.X",
        "X.X..D.XXX.XXXXLX",
        "X.X..X.XXX.XXXX.X",
        "X.XXXX.XXX......X",
        "X......XXX.XX.X.X",
        "X.XXX.........X.X",
        "X.XXX.XXX.XXXXX.X",
        "X.XX............X",
        "X....XXXX.XXKXX.X",
        "X.XXXXXXX.XMMMXLX",
        "X.XXXXXXX.XXXXX.X",
        "XL..............X",
        "XXXXXXXXXXXXXXXXX"
    }
    local string_grid4 = {
        "XXXXXXXXXXXXXXXXX",
        "X...XXXXX.......X",
        "X.X.......XLXXX.X",
        "X.XXX.XXXXX.XXX.X",
        "X.X.........XXX.X",
        "X.L.XXKXX.X.XXX.X",
        "X.X.XMMMX.X.....X",
        "X.X.XXXXX.XXXXX.X",
        "X.X.............X",
        "X.XX.XXX.XXXX.X.X",
        "X.....LX.X..D.X.X",
        "X.XXXX.X.X..X.X.X",
        "X.XXXX.X.XXXX.X.X",
        "X............L..X",
        "XXXXXXXXXXXXXXXXX"
    }
    local string_grid5 = {
        "XXXXXXXXXXXXXXXXX",
        "X....L.........LX",
        "X.XXX.XXXX.XXXX.X",
        "X.XXX.XXXX.X..X.X",
        "X.XXX...XX.X..D.X",
        "X.....X.XX.XXXX.X",
        "X.XXX.X.........X",
        "X.XXX.XXX.XXXX.XX",
        "X.XXX.X........XX",
        "X.......XXXX.X.XX",
        "X.XXKXX.XXXX.X.XX",
        "X.XMMMX.XXX.LX..X",
        "X.XXXXX.XXX.XXX.X",
        "XL..............X",
        "XXXXXXXXXXXXXXXXX"
    }
    local string_grid6 = {
        "XXXXXXXXXXXXXXXXX",
        "X..............LX",
        "X.XXXXX.XXX.XXX.X",
        "X.XXXXX.XXX.XXX.X",
        "X.........X.XXX.X",
        "XXXX.XXXX.X.XXX.X",
        "XXXX.XXXX.......X",
        "XXXX.XXXX.XXKXX.X",
        "X.........XMMMX.X",
        "X.XXXX.XX.XXXXX.X",
        "X.D..X.XX..L....X",
        "X.X..X.XXXX.XXX.X",
        "X.XXXX.XXXX.XXX.X",
        "XL.............LX",
        "XXXXXXXXXXXXXXXXX"
    }

    -- 測試用關卡
    local test_string_grid = {
        "XXXX",
        "X..X",
        "XXXX"
    }

    string_grid = {string_grid1, string_grid2, string_grid3, string_grid4, string_grid4, string_grid5, string_grid6}
    -- string_grid[CurrentLevel] = test_string_grid
    
    -- 根據currentLevel 讀取不同的關卡
    local n = #string_grid[CurrentLevel]
    local m = #string_grid[CurrentLevel][1]

    -- 根據螢幕大小 與 地圖是幾乘幾的，算出一格地圖格在螢幕上，要顯示多大
    GridSize = math.min(display.contentWidth / (m + 1), display.contentHeight / (n + 1))
    
    
    -- 根據currentLevel 讀取不同的背景
    local background = display.newImage(self.view, ImageDir .. CurrentLevel .. "3.png")
    
    -- 地圖的位置與長寬
    background.x = (m + 1) * GridSize / 2
    background.y = (n + 1) * GridSize / 2 + Offest
    background.width = m * GridSize
    background.height = n * GridSize

    -- 把地圖格從，字串轉成二維陣列
    for i = 1, n do
        Grid[i] = {}
        for j = 1, m do
            Grid[i][j] = string_grid[CurrentLevel][i]:sub(j, j)
        end
    end

    for i = 1, n do
        Bean[i] = {} -- 把豆子也表示成二維陣列
        for j = 1, m do
            -- 依照座標(j, i) 算出在螢幕上的座標(X, Y)
            X = j * GridSize
            Y = i * GridSize
            if (Grid[i][j] == 'X') then
                -- 產生障礙物
                local rect = display.newImageRect(self.view, ImageDir .. CurrentLevel .. "1.png", GridSize, GridSize)
                rect.x = X
                rect.y = Y + Offest
                -- rect:setFillColor(0, 1, 1)
            elseif (Grid[i][j] == 'D') then
                -- 產生門
                Grid[i][j] = 'X'
                Door = display.newImageRect(self.view, ImageDir .. "door.png", GridSize, GridSize)
                Door.x = X
                Door.y = Y + Offest
                
                -- 紀錄門在第幾的row 與 第幾個column，其他物件同理
                DoorX = j
                DoorY = i
            elseif (Grid[i][j] == 'K') then
                -- 產生鑰匙
                Key = display.newImageRect(self.view, ImageDir .. "key.png", GridSize, GridSize / 2)
                Key.x = X
                Key.y = Y + Offest
                KeyX = j
                KeyY = i
            elseif (Grid[i][j] == 'L') then
                -- 產生葉子
                LeafCount = LeafCount + 1
                Leaf[LeafCount] = display.newImageRect(self.view, ImageDir .. CurrentLevel .. "2.png", GridSize, GridSize)
                Leaf[LeafCount].x = X
                Leaf[LeafCount].y = Y + Offest
                LeafX[LeafCount] = j
                LeafY[LeafCount] = i
            elseif (Grid[i][j] ~= 'M') then
                -- 產生豆子
                Bean[i][j] = display.newImageRect(self.view, ImageDir .. "bean.png", GridSize / 3, GridSize / 3)
                Bean[i][j].x = X
                Bean[i][j].y = Y + Offest
                BeanCount = BeanCount + 1
            end
        end
    end

    for i = 1, n do
        for j = 1, m do
            X = j * GridSize
            Y = i * GridSize
            if (Grid[i][j] == 'M') then
                -- 產生怪物

                -- 怪物的圖檔 有四種
                local monsterImage = { "M1.png", "M2.png", "M3.png", "M4.png"}
                
                MonsterCount = MonsterCount + 1     -- 怪物的數量 +1
                -- 將第MonsterCount隻的資訊存起來
                Monster[MonsterCount] = display.newImageRect(self.view, ImageDir .. monsterImage[MonsterCount % 3 + 1], GridSize, GridSize)
                MonsterWeak[MonsterCount] = display.newImageRect(self.view, ImageDir .. "M0.png", GridSize, GridSize)
                Monster[MonsterCount].x = X
                Monster[MonsterCount].y = Y + Offest
                MonsterX[MonsterCount] = j
                MonsterY[MonsterCount] = i
                MonsterDirectionX[MonsterCount] = 0 -- 將怪物的方向初始化為 (0, 0)
                MonsterDirectionY[MonsterCount] = 0
                MonsterDead[MonsterCount] = false   -- 怪物都活著
            end
        end
    end


    -- 按鈕的function，上(0, -1) 下(0, 1) 左(-1, 0) 右(0, -1)
    -- 用 NextDirection 紀錄玩家最後按的方向

    local function press_up(event)
        NextDirectionX = 0
        NextDirectionY = -1
    end
    
    local function press_down(event)
        NextDirectionX = 0
        NextDirectionY = 1
    end
    
    local function press_left(event)
        NextDirectionX = -1
        NextDirectionY = 0
    end
    
    local function press_right(event)
        NextDirectionX = 1
        NextDirectionY = 0
    end

    -- 左上角menu選單的按鈕，前往選單的function
    function go_list(event)
        if (event.phase == "began") then
            timer.cancel(MoveTimer) -- 停止呼叫 move
            options = {
                time = 300,
                effect = "fade"
            }
            composer.gotoScene(SrcDir .. "level_select", options)
        end
    end

    -- 玩家的初始位置在(2, 2) 也就是地圖的左上角
    PlayerX = 2
    PlayerY = 2

    Player = display.newImageRect(self.view, ImageDir .. "C2.png", GridSize, GridSize)
    Player.x = PlayerX * GridSize
    Player.y = PlayerY * GridSize + Offest

    -- 紀錄分數與顯示分數
    local prefixScore = display.newText(self.view, "Score: ", display.contentWidth * (7 / 10), Offest - 35, "Minecraft", 100)
    ScoreText = display.newText(self.view, Score, display.contentWidth * (9 / 10), Offest - 35, "Minecraft", 100)
    local levelText = display.newText(self.view, "Level " .. CurrentLevel, display.contentWidth * (2 / 10), Offest - 35, "Minecraft", 100)

    -- 上下左右四個按鈕的擺放
    local offest = 0
    local space = display.contentWidth / 4

    -- 按鈕的設置
    options = {
        fontSize = "50",
        labelColor = { default = { 1, 1, 1 } },
        emboss = false,
        defaultFile = ImageDir .. CurrentLevel .. "4.png",
        overFile = ImageDir .. CurrentLevel .. "5.png",
        width = space,
        height = space,
        alpha = 0.75,
        fillColor = { default = { 0, 0, 0, 0.75 }, over = { 0, 0, 0 } },
        strokeColor = { default = { 0, 0.8, 1, 0.95 }, over = { 1, 1, 0 } },
        strokeWidth = 5
    }

    -- 四個按鈕的中心
    local buttonCenterX = display.contentCenterX
    local buttonCenterY = display.contentCenterY + display.contentHeight / 4

    -- 按鈕外框的圈圈
    local button_circle = display.newImageRect(self.view, ImageDir .. CurrentLevel .. "6.png", space * 2.93, space * 2.93);
    button_circle.x = buttonCenterX
    button_circle.y = buttonCenterY
    button_circle.alpha = 0.75 -- 透明

    -- 上的按紐
    options["onPress"] = press_up
    local button_up = widget.newButton(options)
    -- button_up:setLabel("up")
    button_up.x = buttonCenterX
    button_up.y = buttonCenterY - space - offest    -- 距離中心往上一點
    options["onPress"] = press_down

    -- 下的按鈕
    local button_down = widget.newButton(options)
    -- button_down:setLabel("down")
    button_down.x = buttonCenterX
    button_down.y = buttonCenterY + space + offest  -- 距離中心往下一點
    button_down.rotation = 180
    
    -- 左的按鈕
    options["onPress"] = press_left
    local button_left = widget.newButton(options)
    -- button_left:setLabel("left")
    button_left.x = buttonCenterX - space - offest  -- 距離中心往左一點
    button_left.y = buttonCenterY
    button_left.rotation = 270
    
    -- 右的按鈕
    options["onPress"] = press_right
    local button_right = widget.newButton(options)
    -- button_right:setLabel("right")
    button_right.x = buttonCenterX + space + offest -- 距離中心往右一點
    button_right.y = buttonCenterY
    button_right.rotation = 90

    -- 左上角 menu 的按鈕(文字的部分)
    options = {
        label = "menu",
        fontSize = "50",
        labelColor = { default = { 1, 1, 1 } },
        emboss = false,
        shape = "Rect",
        width = 300,
        height = 75,
        x = 200,
        y = 50,
        onPress = go_list,
        cornerRadius = 2,
        fillColor = { default = { 0, 0, 0, 0.75 }, over = { 0, 0, 0 } },
        strokeColor = { default = { 0, 0.8, 1, 0.95 }, over = { 1, 1, 0 } },
        strokeWidth = 5
    }
    local button_leave = widget.newButton(options)
    
    -- 左上角 menu 的按鈕(圖片的部分)
    options = {
        defaultFile = ImageDir .. "home.jpg",
        width = 50,
        height = 50,
        x = 100,
        y = 50,
        onPress = go_list
    }
    local home = widget.newButton(options)

    self.view:insert(button_up)
    self.view:insert(button_down)
    self.view:insert(button_left)
    self.view:insert(button_right)
    self.view:insert(button_leave)
    self.view:insert(home)
end

function scene:show(event)
    if (event.phase == "will") then

        bgm = audio.loadSound(SoundDir .. "bgm.mp3") -- 遊玩時的bgm
        
        -- 先清空channel
        audio.stop(3)
        audio.stop(4)

        -- 播放 bgm
        audio.play(bgm, {channel = 3, loops = -1})

        -- 最重要的function，會不斷執行，讓玩家與怪物往前
        -- 若 FPS = 30，則呼叫一次move會往前 1 / 30 格
        function move()

            --------------------
            -- 小精靈碰到各種物件
            --------------------

            local c = Grid[PlayerY][PlayerX] -- 玩家所在的位置的物件，也就是玩家碰到什麼物件
            if (c == '.') then
                -- 碰到豆子
                local bean_sound = audio.loadSound(SoundDir .. "bean.mp3") -- 音效
                audio.play(bean_sound)
                Grid[PlayerY][PlayerX] = 'S'    -- 標記為 Space 讓玩家下次經過同樣的格子時，不會再次進入這個if
                Bean[PlayerY][PlayerX].isVisible = false -- 隱藏豆子
                BeanCount = BeanCount - 1       -- 剩餘的豆子數量減少
                Score = Score + 4               -- 分數 +4
                ScoreText.text = Score          -- 顯示分數
                
                -- 贏的時候，前往下一關的function，給button使用
                local function win()
                    options = {
                        time = 300,
                        effect = "fade"
                    }
                    audio.stop(2)
                    if (CurrentLevel ~= 6) then
                        -- 根據 currentLevel 前往下一關
                        composer.gotoScene(SrcDir .. "level" .. (CurrentLevel + 1), options)
                    else
                        -- 若次關為最後一關，則回到title
                        composer.gotoScene(SrcDir .. "title", options)
                    end
                end

                if (BeanCount == 0) then
                    -- 若豆子數量剩餘0，代表贏了

                    local win_sound = audio.loadSound(SoundDir .. "win.mp3") -- 播放音效
                    winImage = display.newImageRect(self.view, ImageDir .. "win.png", display.contentWidth, display.contentHeight) -- 顯示congratulation
                    winImage.x = display.contentCenterX
                    winImage.y = display.contentCenterY
                    audio.stop(3)                           -- 停止播放bgm
                    audio.play(win_sound, {channel = 5})    -- 放贏了的音樂
                    timer.cancel(MoveTimer)                 -- 停止執行move

                    -- next level 的按鈕
                    options = {
                        label = "Next Level",
                        fontSize = "75",
                        labelColor = { default = { 1, 1, 0 } },
                        emboss = false,
                        shape = "Rect",
                        x = display.contentCenterX,
                        y = display.contentCenterY * 1.5,
                        width = 1000,
                        height = 200,
                        cornerRadius = 2,
                        fillColor = { default = { 0, 0, 0 }, over = { 0, 0, 0 } },
                        strokeColor = { default = { 1, 0.8, 0, 0.95 }, over = { 1, 1, 0 } },
                        strokeWidth = 5,
                        onPress = win
                    }
                    if (CurrentLevel == 6) then
                        -- 若此關為最後一關，則沒有下一關了，顯示all pass，回到選單
                        options["label"] = "All Pass !!"
                    end
                    local button_win = widget.newButton(options)
                    self.view:insert(button_win)
                end
            elseif (c == 'K') then
                -- 碰到鑰匙
                local key_sound = audio.loadSound(SoundDir .. "key.mp3") -- 音效
                audio.play(key_sound)
                Grid[PlayerY][PlayerX] = 'S'    -- 將這格標記為 space
                Key.isVisible = false           -- 把門與鑰匙隱藏
                Door.isVisible = false
                Grid[DoorY][DoorX] = 'S'        -- 將門的地方設為可以行走
            elseif (c == 'L') then
                -- 碰到葉子
                local leaf_sound = audio.loadSound(SoundDir .. "leaf.mp3")
                audio.play(leaf_sound)
                Grid[PlayerY][PlayerX] = 'S'    -- 將這格標記為 space
                for i = 1, LeafCount do
                    if (LeafX[i] == PlayerX and LeafY[i] == PlayerY) then
                        Leaf[i].isVisible = false                       -- 將葉子隱藏
                        LeafRemainTime = LeafRemainTime + (FPS * 10)    -- 延長葉子的持續時間(持續十格)
                    end
                end
            end

            if (LeafRemainTime > 0) then
                -- 若葉子的效力還在 則顯示黑色的弱小怪物
                for i = 1, MonsterCount do
                    -- 將每隻怪獸顯示的樣子設定成黑色弱小怪物
                    MonsterWeak[i].x = Monster[i].x
                    MonsterWeak[i].y = Monster[i].y
                    Monster[i].isVisible = false
                    if (MonsterDead[i]) then
                        MonsterWeak[i].isVisible = false
                    else
                        MonsterWeak[i].isVisible = true
                    end
                end
                LeafRemainTime = LeafRemainTime - 1
            else
                -- 若此時無葉子效力 則顯示平時的怪物
                for i = 1, MonsterCount do
                    -- 將每隻怪獸顯示的樣子設定成平時的怪物
                    MonsterWeak[i].isVisible = false
                    if (MonsterDead[i] == true) then
                        Monster[i].isVisible = false
                    else
                        Monster[i].isVisible = true
                    end
                end
            end


            ----------------
            -- 決定小精靈方向
            ----------------

            -- (nextX, nextY) 為小精靈下一格的座標
            local nextX = PlayerX + DirectionX
            local nextY = PlayerY + DirectionY
            
            -- 若小精靈是有方向的(不是停止的狀態) 而且 下一格不是障礙物，則往那個方向走 (1 / FPS) 格
            -- 若 FPS = 30，則呼叫一次move會走 1 / 30 格
            if ((DirectionX ~= 0 or DirectionY ~= 0) and Grid[nextY][nextX] ~= 'X') then
                Player.x = Player.x + DirectionX * (GridSize / FPS)
                Player.y = Player.y + DirectionY * (GridSize / FPS)
                StepCount = StepCount + 1 -- 紀錄 往前走了一步
            else
                DirectionX = NextDirectionX
                DirectionY = NextDirectionY
            end

            if (StepCount == FPS) then
                -- 到達下一格了(若FPS = 30，則這裡代表已經走30步了)
                PlayerX = nextX -- 更新玩家現在的座標
                PlayerY = nextY
                
                -- 檢查玩家所按的方向，看是否能往那個方向走
                if (Grid[PlayerY + NextDirectionY][PlayerX + NextDirectionX] ~= 'X') then
                    -- 若可以往那個方向走，更新小精靈的方向
                    DirectionX = NextDirectionX
                    DirectionY = NextDirectionY
                end
                StepCount = 0
            end

            -- 根據小精靈的方向 讓小精靈的圖檔面向前方(上下左右旋轉)
            if (DirectionX == 1) then
                Player.xScale = -1
                Player.rotation = 0 
            elseif (DirectionX == -1) then
                Player.xScale = 1
                Player.rotation = 0 
            elseif (DirectionY == 1) then
                Player.xScale = 1
                Player.rotation = 270 
            elseif (DirectionY == -1) then
                Player.xScale = 1
                Player.rotation = 90               
            end


            ----------------
            -- 決定怪物的方向
            ----------------

            for i = 1, MonsterCount do

                -- 第i隻怪物的 下一格的位置
                local MnextX = MonsterX[i] + MonsterDirectionX[i]
                local MnextY = MonsterY[i] + MonsterDirectionY[i]

                if (MonsterStep % FPS == 0) then
                    -- 若怪物已走到了下一格

                    MonsterX[i] = MnextX -- 更新怪物位置
                    MonsterY[i] = MnextY

                    -- 第i隻怪物的 下一格的位置
                    MnextX = MonsterX[i] + MonsterDirectionX[i]
                    MnextY = MonsterY[i] + MonsterDirectionY[i]

                    local isVertical = (Grid[MonsterY[i] - 1][MonsterX[i]] ~= 'X' or Grid[MonsterY[i] + 1][MonsterX[i]] ~= 'X') -- 上或下 可以走
                    local isHorizontal = (Grid[MonsterY[i]][MonsterX[i] - 1] ~= 'X' or Grid[MonsterY[i]][MonsterX[i] + 1] ~= 'X') -- 左或右可以走
                    
                    -- isVertical and isHorizontal 就是 此格是岔路
                    -- Grid[MnextY][MnextX]) == 'X' 就是 下一格是障礙物
                    -- MonsterDirectionX[i] == 0 and MonsterDirectionY[i] == 0 就是 怪物是停止的
                    
                    -- 若發生以上三種情形之一，就要重新決定怪物的方向 會進入while迴圈
                    while (isVertical and isHorizontal) or (Grid[MnextY][MnextX]) == 'X' or (MonsterDirectionX[i] == 0 and MonsterDirectionY[i] == 0) do
                        local rand1 = math.random(0, 1) -- rand1 決定要走 垂直方向 或是 水平方向
                        local rand2 = math.random(0, 1) -- rand2 決定要走 往座標軸的正向(下右)還是負向(上左)
                        local rand3 = math.random(0, 9) -- rand3 決定是否停止移動(10%的機率)
                        MonsterDirectionX[i] = rand1
                        MonsterDirectionY[i] = (1 - rand1)
                        if (rand2 ~= 0) then
                            MonsterDirectionX[i] = MonsterDirectionX[i] * -1
                            MonsterDirectionY[i] = MonsterDirectionY[i] * -1
                        end
                        if (rand3 == 9) then
                            MonsterDirectionX[i] = 0
                            MonsterDirectionY[i] = 0
                        end

                        MnextX = MonsterX[i] + MonsterDirectionX[i]
                        MnextY = MonsterY[i] + MonsterDirectionY[i]

                        if Grid[MnextY][MnextX] ~= 'X' then
                            -- 若下一格不是障礙物，就跳出迴圈
                            break
                        end
                        -- 若下一格是障礙物，就重新決定方向
                    end
                end
                
                MnextX = MonsterX[i] + MonsterDirectionX[i]
                MnextY = MonsterY[i] + MonsterDirectionY[i]
                
                -- 若下一格不是障礙物，則往前走(1 / FPS)步
                if (Grid[MnextY][MnextX] ~= 'X') then
                    Monster[i].x = Monster[i].x + MonsterDirectionX[i] * (GridSize / FPS)
                    Monster[i].y = Monster[i].y + MonsterDirectionY[i] * (GridSize / FPS)
                end
            end
            
            -- 紀錄往前走了一步
            MonsterStep = MonsterStep + 1
            
            local touch = false -- 存 是否被怪物殺死了
            for i = 1, MonsterCount do
                local distance = math.abs(Monster[i].x - Player.x) + math.abs(Monster[i].y - Player.y) -- 算出與怪物的距離
                if (distance <= GridSize / 2) then
                    -- 若距離太近，視為碰撞

                    -- 檢查現在是否有葉子的效力
                    if (LeafRemainTime > 0 and MonsterDead[i] == false) then
                        -- 若有葉子的效力，則將怪物隱藏
                        local touch_sound = audio.loadSound(SoundDir .. "touch.mp3") -- 音效
                        audio.play(touch_sound)
                        Score = Score + 100             -- 加分
                        ScoreText.text = Score          -- 更新分數
                        MonsterDead[i] = true           -- 將怪物標記成死亡
                        Monster[i].isVisible = false    -- 隱藏
                    elseif (MonsterDead[i] == false) then
                        -- 若無葉子的效力，則玩家被怪物殺死
                        touch = true
                    end
                end
            end

            -- 輸掉，點擊為到選單的function
            local function lose()
                options = {
                    time = 300,
                    effect = "fade"
                }
                audio.stop(2)
                composer.gotoScene(SrcDir .. "level_select", options)
            end
            if (touch) then
                -- 若玩家被怪物殺死
                local fail_sound = audio.loadSound(SoundDir .. "fail.mp3") -- 音效
                failImage = display.newImageRect(self.view, ImageDir .. "gameover.png", display.contentWidth, display.contentHeight) -- Game Over的圖片
                failImage.x = display.contentCenterX
                failImage.y = display.contentCenterY
                audio.stop(3)
                audio.play(fail_sound, {channel = 4}) -- 播放音效
                timer.cancel(MoveTimer) -- 停止呼叫 move()
                options = {
                    label = "T a p   T o   C o n t i n u e",
                    fontSize = "65",
                    font = "Minecraft",
                    labelColor = { default = { 1, 1, 1 } },
                    emboss = false,
                    shape = "Rect",
                    x = display.contentCenterX,
                    y = display.contentCenterY,
                    width = display.contentWidth / (1.5),
                    height = display.contentHeight / 10,
                    cornerRadius = 2,
                    fillColor = { default = { 0, 0, 0 }, over = { 0, 0, 0 } },
                    strokeColor = { default = { 1, 0, 0, 0.95 }, over = { 1, 1, 0 } },
                    strokeWidth = 20,
                    onPress = lose
                }
                local button_lose = widget.newButton(options)
                self.view:insert(button_lose)
            end
        end

        -- timer.performWithDelay(OneGridPerSecond * 1000 / FPS, move, -1)

        MoveTimer = timer.performWithDelay(Delay, move, -1)
    elseif (event.phase == "did") then
    end
end

function scene:hide(event)
    if (event.phase == "will") then
        -- 場景結束 音效停止
        audio.stop(2)
        audio.stop(3)
        audio.stop(4)
        audio.stop(5)
        audio.dispose(bgm)
        composer.removeScene(SrcDir .. "level" .. CurrentLevel)
    elseif (event.phase == "did") then
    end
end

function scene:destrory(event)
    self.view:removeSelf()
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destrory", scene)

return scene