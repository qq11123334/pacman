local widget    = require("widget")
local movieclip = require("movieclip")
local physics   = require("physics")

local scene = composer.newScene()

function scene:create(event)
	composer.removeScene(SrcDir .. "title");
	composer.removeScene(SrcDir .. "level1")
	composer.removeScene(SrcDir .. "level2")
	composer.removeScene(SrcDir .. "level3")
	composer.removeScene(SrcDir .. "level4")
	composer.removeScene(SrcDir .. "level5")
	composer.removeScene(SrcDir .. "level6")

	display.setStatusBar(display.HiddenStatusBar)

	-- 背景
	local background = display.newImageRect(self.view, ImageDir .. "background.png", display.contentWidth, display.contentHeight)
	background.x = display.contentCenterX
	background.y = display.contentCenterY

	-- 點擊關卡按鈕時的音效
	local tap_sound = audio.loadSound(SoundDir .. "tap.mp3")
	
	-- 進入 1 ~ 6 關的function
	function go_level1(event)
		if (event.phase == "began") then
			audio.play(tap_sound)
			options = {
				time = 300,
				effect = "fade"
			}
			composer.gotoScene(SrcDir .. "level1", options)
		end
	end
	function go_level2(event)
		if (event.phase == "began") then
			audio.play(tap_sound)
			options = {
				time = 300,
				effect = "fade"
			}
			composer.gotoScene(SrcDir .. "level2", options)
		end
	end
	function go_level3(event)
		if (event.phase == "began") then
			audio.play(tap_sound)
			options = {
				time = 300,
				effect = "fade"
			}
			composer.gotoScene(SrcDir .. "level3", options)
		end
	end
	function go_level4(event)
		if (event.phase == "began") then
			audio.play(tap_sound)
			options = {
				time = 300,
				effect = "fade"
			}
			composer.gotoScene(SrcDir .. "level4", options)
		end
	end
	function go_level5(event)
		if (event.phase == "began") then
			audio.play(tap_sound)
			options = {
				time = 300,
				effect = "fade"
			}
			composer.gotoScene(SrcDir .. "level5", options)
		end
	end
	function go_level6(event)
		if (event.phase == "began") then
			audio.play(tap_sound)
			options = {
				time = 300,
				effect = "fade"
			}
			composer.gotoScene(SrcDir .. "level6", options)
		end
	end

	-- 1 ~ 6 關的按鈕的options(外觀)
	options = {
		fontSize = "150",
		emboss = false,
		-- Properties for a rounded rectangle button
		shape = "roundedRect",
		width = display.contentWidth / 5,
		height = display.contentWidth / 5,
		cornerRadius = 2,
		fillColor = { default = { 0, 0, 0 }, over = { 0, 0, 0 } },
		strokeColor = { default = { 0, 0.8, 1, 0.95 }, over = { 1, 1, 0 } },
		strokeWidth = 4
	}

	-- 1 ~ 6 關的位置
	-- xRatio 表示 位在螢幕 x軸 的幾分之幾(最左邊是0 最右邊是1)
	-- yRatio 表示 位在螢幕 y軸 的幾分之幾(最上面是0 最下面是1)
	-- 有六個按鈕 以 2 * 3 的方式擺放
	local xRatio = {1 / 5, 1 / 2, 4 / 5}
	local yRatio = {2 / 3, 6 / 7}
	local press = {go_level1, go_level2, go_level3, go_level4, go_level5, go_level6}
	for i = 1, 2 do
		for j = 1, 3 do
			-- 設定每個按鈕(上面的字 位置 要執行的function)
			-- 根據xRatio yRatio擺放按鈕
			local x = xRatio[j] * display.contentWidth
			local y = yRatio[i] * display.contentHeight
			local label = (i - 1) * 3 + j
			options["onPress"] = press[label]
			options["label"] = tostring(label)
			local button = widget.newButton(options)
			button.x = x
			button.y = y
			self.view:insert(button)
		end
	end

end

function scene:show(event)
	if (event.phase == "will") then
	elseif (event.phase == "did") then
	end
end

function scene:hide(event)
	if (event.phase == "will") then
		composer.removeScene(SrcDir .. "level_select")
	elseif (event.phase == "did") then
	end
end

function scene:destrory(event)
	self.view:removeSelf()
	self.view = nil
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destrory", scene)

return scene
